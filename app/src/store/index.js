import { combineReducers, createStore, applyMiddleware } from 'redux'
import ReduxThunk from 'redux-thunk'
import sugokuReducer from './reducers/sugokuReducer'

const reducers = combineReducers({
  sugokuReducer
})

const store = createStore(reducers, applyMiddleware(ReduxThunk))
export default store