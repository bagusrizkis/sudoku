import {
  SET_BOARD,
  SET_STATUS,
  SET_SOLUTION,
  SET_LEADERBOARD
} from ".";

const encodeBoard = (board) => board.reduce((result, row, i) => result + `%5B${encodeURIComponent(row)}%5D${i === board.length - 1 ? '' : '%2C'}`, '')

const encodeParams = (params) => {
  return Object.keys(params)
    .map(key => key + '=' + `%5B${encodeBoard(params[key])}%5D`)
    .join('&');
}

const baseURL = 'https://sugoku.herokuapp.com'

export function setBoard(payload) {
  return {
    type: SET_BOARD,
    payload: payload
  }
}

export function setSolution(payload) {
  return {
    type: SET_SOLUTION,
    payload: payload
  }
}

export function setStatus(payload) {
  return {
    type: SET_STATUS,
    payload: payload
  }
}

export function setLeaderBoard(payload) {
  console.log('actin', payload)
  return {
    type: SET_LEADERBOARD,
    payload: payload
  }
}

export function fetchBoard(level = 'easy') {
  console.log('action ->>>', level)
  let url;
  if (level !== 'blank') {
    url = baseURL + '/board?difficulty=' + level.toLowerCase()
  } else {
    url = baseURL + '/board'
  }

  return (dispatch) => {
    fetch(url)
      .then(res => res.json())
      .then(data => {
        dispatch(setBoard(data.board))
      })
      .catch(err => console.log('dari action fetchBoard', err))
  }
}

export function autosolve(board) {
  const data = {
    board: board
  }

  return (dispatch) => {
    fetch(baseURL + '/solve', {
      method: 'POST',
      body: encodeParams(data),
      headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
    })
      .then(response => response.json())
      .then(data => {
        if (data.status === 'solved') {
          dispatch(setSolution(data.solution))
        } else {
          dispatch(setStatus(data.status))
        }
      })
      .catch(console.warn)
  }
}

export function validate(board) {
  const data = {
    board: board
  }

  return (dispatch) => {
    fetch(baseURL + '/validate', {
      method: 'POST',
      body: encodeParams(data),
      headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
    })
      .then(response => response.json())
      .then(data => {
        // console.log('dari action >>>>>', data, board)
        dispatch(setStatus(data.status))
      })
      .catch(console.warn)
  }
}