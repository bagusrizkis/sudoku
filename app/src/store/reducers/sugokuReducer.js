import { SET_BOARD, SET_STATUS, SET_SOLUTION, SET_LEADERBOARD } from '../actions'
import { board0 } from './board0'

const initState = {
  board: [...board0],
  solution: [...board0],
  status: '',
  leaderBoard: []
}

export default (state = initState, action) => {
  switch (action.type) {
    case SET_BOARD:
      return { ...state, board: action.payload }
    case SET_SOLUTION:
      return { ...state, solution: action.payload }
    case SET_STATUS:
      console.log(']]]]]]', action.payload)
      return { ...state, status: action.payload }
      case SET_LEADERBOARD:
        // let newArray = [...state.leaderBoard, ...action.payload]
        // console.log('okeok', newArray.sort((a,b) =>  a.realPlayTimeSecond - b.realPlayTimeSecond ))
        // console.log('<<<<<><><><><', action.payload)
      return { ...state, leaderBoard: state.leaderBoard.concat(action.payload)}
    default:
      return state
  }
}