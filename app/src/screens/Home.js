import { LinearGradient } from 'expo-linear-gradient'
import React, { useState } from 'react'
import { Text, View, StyleSheet, Alert, Image } from 'react-native'
import { TextInput, TouchableOpacity } from 'react-native-gesture-handler'

export default function Home(props) {
  const [ level, setLevel ] = useState('')
  const [ playerName, setPlayerName ] = useState('')
  const [ levelTime, setLevelTime ] = useState(0)
  
  
  const optionLevel = [
    { name: 'Easy', levelTime: 600 },
    { name: 'Medium', levelTime: 300 },
    { name: 'Hard', levelTime: 120 },
  ]

  const inputPlayerName = (text) => {
    setPlayerName(text)
  }

  const startGame = () => {
    if (level === '') {
      Alert.alert(
        'Hmmm',
        'Pilih Level untuk memulai Sudoku',
        [
          { text: "OK" }
        ])
    } else if (playerName === '') {
      Alert.alert(
        'Hmmm',
        'Nama gak boleh kosong',
        [
          { text: "OK" }
        ])
    } else {
      setPlayerName('')
      props.navigation.navigate('Game', {
        level: level,
        playerName: playerName,
        levelTime: levelTime
      })
    }
  }

  return (
    <>
      <LinearGradient
        colors={['#8BC6EC', '#9599E2']}
        style={styles.container}
      >
        <View style={styles.container}>
          {/* Title */}
          <View style={{ marginTop: 30 }}>
            <Text style={styles.title}>SUdoKU</Text>
          </View>
          {/* Main */}

          <View style={{ marginTop: 0, marginBottom: 100 }}>
            <View style={{height: 100, width: 100}}>
              {/* <Image
                width='200%'
                resizeMode='contain'
                source={require('../assets/logo.png')}
              /> */}
            </View>
            <TextInput
              style={styles.playerNameInput}
              onChangeText={text => inputPlayerName(text)}
              value={playerName}
              placeholder='Enter Name'
              caretHidden={true}
            >
            </TextInput>
            <View style={styles.levelButton}>
              {
                optionLevel.map((el, idx) => {
                  return <TouchableOpacity
                    key={idx}
                    style={[styles.primaryButton, { backgroundColor: level === el.name ? '#e94560' : "#07689f" }]}
                    onPress={() => { setLevel(el.name); setLevelTime(el.levelTime) }}
                  >
                    <Text style={styles.appButtonText}>{el.name}</Text>
                  </TouchableOpacity>
                })
              }
            </View>
          </View>
          {/* Bottom */}
          <View>
            <TouchableOpacity
              style={[styles.primaryButton, { backgroundColor: "#07689f" }]}
              onPress={startGame}
            >
              <Text style={styles.appButtonText}>Start Game</Text>
            </TouchableOpacity>
          </View>
        </View>
      </LinearGradient>
    </>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
  },

  title: {
    fontSize: 50,
    fontWeight: 'bold'
  },

  playerNameInput: {
    height: 50,
    borderBottomColor: 'white',
    borderBottomWidth: 4,
    fontSize: 40,
    textAlign: 'center',
    margin: 20,
  },

  primaryButton: {
    elevation: 8,
    borderRadius: 10,
    paddingVertical: 10,
    paddingHorizontal: 12,
    minWidth: 100,
  },


  levelButton: {
    flexDirection: 'row',
    justifyContent: 'space-around',
  },

  appButtonText: {
    fontSize: 18,
    color: "#fff",
    fontWeight: "bold",
    alignSelf: "center",
    textTransform: "uppercase"
  }
});
