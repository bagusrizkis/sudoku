import { LinearGradient } from 'expo-linear-gradient'
import React, { useEffect, useState } from 'react'
import { Text, View, StyleSheet, FlatList } from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler'
import { useDispatch, useSelector } from 'react-redux'
import { setLeaderBoard, setStatus } from '../store/actions/sugokuAction'

function Finish (props) {
  const { leaderBoard } = useSelector(state => state.sugokuReducer)

  const { route } = props

  const gameResult = {
    time: route.params.playTime,
    playerName: route.params.playerName,
    level: route.params.level,
    finishAt: new Date(),
    realPlayTimeSecond: route.params.realPlayTimeSecond
  }
  const dispatch = useDispatch()

  useEffect(() => {
    if (route.params.status !== 'loser') {
      dispatch(setLeaderBoard(gameResult))
    }
    dispatch(setStatus('unsolved'))
  }, [])

  useEffect(() => {
    // console.log(leaderBoard)
  }, [leaderBoard])

  const startAgain = () => {
    props.navigation.replace('Game', {
      level: gameResult.level,
      playerName: gameResult.playerName,
      levelTime: route.params.levelTime
    })
  }

  const backToHome = () => {
    props.navigation.replace('Home')
  }

  return (
    <>
      <LinearGradient
        colors={['#8BC6EC', '#9599E2']}
        style={styles.container}
      >
        <View style={[styles.container]}>
          {/* Title */}
          <View style={{ marginTop: 30 }}>
            <Text style={styles.title}> {route.params.status === 'winner' ? 'Selamat' : 'Sorry'} {route.params.playerName}</Text>
          </View>
          {/* Main */}
          <View style={[{ height: 400}, styles.leaderBoard]}>
            <Text style={styles.leaderBoardTitle}>Board: </Text>
            <FlatList
              data={leaderBoard}
              renderItem={({ item }) =>
                <View style={{ flexDirection: 'row', alignItems: 'center', borderRadius: 15, backgroundColor: '#fff', marginBottom: 4}} key={item.finishAt}>
                  <Text style={[styles.item, { }]}>{item.playerName}</Text>
                  <Text style={[styles.item, { fontSize: 20, textAlign: 'center'}]}>{item.time} m</Text>
                  <Text style={styles.item}>{item.level}</Text>
                </View>
              }
              keyExtractor={(item, index) => index.toString()}
            />
          </View>
          {/* Bottom */}
          <View style={{ flexDirection: 'row', marginTop: 20, justifyContent: 'space-around'}}>
          <TouchableOpacity
              style={[styles.primaryButton, { backgroundColor: "#07689f" }]}
              onPress={startAgain}
            >
              <Text style={styles.appButtonText}>Start Again</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={[styles.primaryButton, { backgroundColor: "#07689f" }]}
              onPress={backToHome}
            >
              <Text style={styles.appButtonText}>Home</Text>
            </TouchableOpacity>
          </View>
        </View>
      </LinearGradient>
    </>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
  },

  title: {
    fontSize: 50,
    fontWeight: 'bold'
  },

  primaryButton: {
    elevation: 8,
    borderRadius: 10,
    paddingVertical: 10,
    paddingHorizontal: 12,
    minWidth: 100,
  },

  leaderBoardTitle: {
    fontSize: 40,
    fontWeight: 'bold',
    padding: 30,
    textAlign: "center"
  },

  leaderBoard: {
    textAlign: 'center'
  },

  item: {
    fontSize: 30,
    margin: 5,

    borderRadius: 5,
    paddingLeft: 3,
    paddingRight: 3,
    fontWeight: '200',
    minWidth: 90,
    height: '80%'
  },

  levelButton: {
    flexDirection: 'row',
    justifyContent: 'space-around',
  },

  appButtonText: {
    fontSize: 18,
    color: "#fff",
    fontWeight: "bold",
    alignSelf: "center",
    textTransform: "uppercase"
  }
});


const mapStateToProps = state => {
  console.log(state)
  return {
    leaderBoard: state.sugokuReducer.leaderBoard
  }
}

export default Finish