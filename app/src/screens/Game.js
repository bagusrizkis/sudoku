import React, { useEffect, useState } from 'react';
import { View, Text, TextInput, StyleSheet, Dimensions, TouchableOpacity, Alert, LogBox } from 'react-native'
import { connect, useDispatch } from 'react-redux'
import { autosolve, fetchBoard, validate, setStatus } from '../store/actions/sugokuAction';
import CountDown from 'react-native-countdown-component';
LogBox.ignoreAllLogs();//Ignore all log notifications
LogBox.ignoreAllLogs();//Ignore all log notifications

const width = Dimensions.get("window").width;
// const screen = Dimensions.get("screen");

let counter;
function Game(props) {
  const { board, status, solution, route } = props
  const [localData, setlocalData] = useState([])
  const [playTime, setPlayTime] = useState(0)

  const dispatch = useDispatch()

  useEffect(() => {
    if (status !== 'solved') {
      counter = setTimeout(() => setPlayTime(playTime + 1), 1000);
    } else {
      return clearInterval(counter);
    }
  }, [playTime]);

  const goToFinishAsLoser = () => {
    props.navigation.replace('Finish', {
      playerName: route.params.playerName,
      status: 'loser'
    })
  }

  useEffect(() => {
    console.log('useEffect')
    dispatch(fetchBoard(route.params.level))
    console.log(route.params.playerName, route.params.levelTime)
  }, [])

  useEffect(() => {
    // let globalBoardSolution = JSON.parse(JSON.stringify(solution))
    // setlocalData(globalBoardSolution)
  }, [solution])

  useEffect(() => {
    if (status === 'solved') {
      dispatch(setStatus(''))
      props.navigation.replace('Finish', {
        playerName: route.params.playerName,
        playTime: `${Math.floor(playTime / 60)}:${playTime%60}`,
        realPlayTimeSecond: playTime,
        level: route.params.level,
        levelTime: route.params.levelTime,
        status: 'winner'
      })
    }
  }, [status])

  useEffect(() => {
    setLocalFromGlobal()
    // change solution fetch here
    dispatch(autosolve(board))
  }, [board])

  const clearBoard = () => {
    setLocalFromGlobal()
  }

  const setLocalFromGlobal = () => {
    let globalBoard = JSON.parse(JSON.stringify(board))
    setlocalData(globalBoard)
  }

  const autosolveHandler = () => {
    // change to global board
    dispatch(autosolve(board)) // unutk jaga-jaga dat agak update
    let globalBoardSolution = JSON.parse(JSON.stringify(solution))
    setlocalData(globalBoardSolution)
  }

  const validateHandler = () => {
    dispatch(validate(localData))
  }

  const handleChangeInput = (e, i_row, i_col) => {
    const newData = [...localData]
    newData[i_row][i_col] = Number(e)
    // console.log('dari local', newData[i_row][i_col])
    // console.log('dari global', board[i_row][i_col])
    setlocalData(newData)
  }

  return (
    <View style={{ marginTop: 20}}>
      <View style={{ justifyContent: 'center', alignItems: 'center', width: width }}>
        <View style={{ height: 100, flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
          <Text
            style={{fontSize: 30}}
          >{(route.params.playerName).toUpperCase()}</Text>
          <CountDown
            style={{ margin: 20 }}
            until={route.params.levelTime}
            size={20}
            onFinish={() => goToFinishAsLoser()}
            digitStyle={{backgroundColor: '#FFF'}}
            digitTxtStyle={{color: '#070d59'}}
            timeToShow={['M', 'S']}
            timeLabels={false}
          />
          <Text
            style={{fontSize: 30}}
          >{route.params.level}</Text>
        </View>
        <View style={styles.board}>
          {
            localData.map((el, i_row) => {
              return (
                <View style={styles.row}
                  key={i_row}
                >
                  {
                    el.map((num, i_col) => {
                      return (
                        <TextInput
                          key={Number(String(i_row) + String(i_col))}
                          keyboardType='numeric'
                          editable={board[i_row][i_col] !== 0 ? false : true}
                          style={[styles.column, {backgroundColor: board[i_row][i_col] !== 0 ? 'rgba(186, 190, 201, 0.4)': localData[i_row][i_col] !== solution[i_row][i_col] && localData[i_row][i_col] !== 0 ? '#ff9a8c' : "rgba(246, 244, 244, 0.7)"}]}
                          value={num !== 0 ? String(num) : ''}
                          maxLength={1}
                          onChangeText={(e) => handleChangeInput(e, i_row, i_col)}
                        >
                        </TextInput>)
                    })
                  }
                </View>
              )
            })
          }
        </View>
      </View>

      <View style={{ margin: 20 }}></View>
      <View style={styles.bottomButton}>
        <TouchableOpacity
          style={[styles.primaryButton, {backgroundColor: "#1f3c88"}]}
          onPress={autosolveHandler}
        >
          <Text style={styles.appButtonText}>auto Solve</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={[styles.primaryButton, {backgroundColor: "#1f3c88"}]}
          onPress={clearBoard}
        >
          <Text style={styles.appButtonText}>Clear</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={[styles.primaryButton, {backgroundColor: "#1f3c88"}]}
          onPress={validateHandler}
        >
          <Text style={styles.appButtonText}>Validate</Text>
        </TouchableOpacity>
      </View>
      <View style={{ margin: 20 }}></View>
    </View>
  )
}

const styles = StyleSheet.create({
  board: {
    backgroundColor: 'rgba(246, 244, 244, 0.7)',
    borderWidth: 2.5,
    borderRadius: 20,
    width: (width - 15),
    height: (width - 15)
  },

  row: {
    flexDirection: 'row'
  },

  column: {
    width: (width - 20) / 9,
    height: (width - 20) / 9,
    borderWidth: 0.5,
    borderColor: '#bbbfca',
    borderRadius: 14,
    fontSize: 25,
    color: '#070d59',
    fontWeight: 'bold',
    textAlign: 'center',
  },

  bottomButton: {
    flexDirection: 'row',
    justifyContent: 'space-around',
  },

  primaryButton: {
    elevation: 8,
    borderRadius: 10,
    paddingVertical: 10,
    paddingHorizontal: 12,
    minWidth: 100,
  },

  appButtonText: {
    fontSize: 18,
    color: "#fff",
    fontWeight: "bold",
    alignSelf: "center",
    textTransform: "uppercase"
  }

})

const mapStateToProps = state => {
  return {
    board: state.sugokuReducer.board,
    solution: state.sugokuReducer.solution,
    status: state.sugokuReducer.status
  }
}

export default connect(mapStateToProps)(Game)