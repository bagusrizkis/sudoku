import React from 'react';
import 'react-native-gesture-handler';
import { LinearGradient } from 'expo-linear-gradient'
import { StatusBar } from 'expo-status-bar';
import { Provider } from 'react-redux';
import { NavigationContainer } from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack'
import { BlurView } from 'expo-blur'
const Stack = createStackNavigator()

import store from './src/store';
import Home from './src/screens/Home';
import Game from './src/screens/Game'
import Finish from './src/screens/Finish'

export default function App() {
  return (
    <Provider store={store}>
      <StatusBar hidden={true}/>
        <NavigationContainer>
          <Stack.Navigator>
            <Stack.Screen
              name='Home'
              component={Home}
              options={{
                headerShown: false
              }}
            />
            <Stack.Screen name='Game' component={Game}
              options={{
                headerShown: false
              }}
            />
            <Stack.Screen name='Finish' component={Finish} 
              options={{
                headerShown: false
              }}
            />
          </Stack.Navigator>
        </NavigationContainer>
    </Provider>
  );
}